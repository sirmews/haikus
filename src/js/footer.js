var React = require('react');
var ReactDOM = require('react-dom');

module.exports = React.createClass({
    render: function(){
        let heartStyle = {
            color: '#be1931'
        };
        return (
            <footer className="pt5 pb4 ph3 ph5-ns">
                <small className="f5 db">A work of Navishkar (Nav) Rao / <a className="link color-inherit underline" href="https://github.com/raonav">Github</a> / <a className="link color-inherit underline" href="mailto:&#114;&#097;&#111;&#110;&#097;&#118;&#064;&#111;&#117;&#116;&#108;&#111;&#111;&#107;&#046;&#099;&#111;&#109;">Email</a></small>
            </footer>
        );
    }
});
