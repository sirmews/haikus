var React = require('react');
var ReactDOM = require('react-dom');
var Haiku = require('./haiku');
var data = require('./data');
var Header = require('./header');
var Footer = require('./footer');

module.exports = React.createClass({
    render: function(){
        var haikuNodes = data.map(function(haiku, key){
            return (
                <Haiku key={key} haiku={haiku}></Haiku>
            );
        }, this);
        return (
            <div className="app">
                <Header/>
                <main role="main" id="main">
                    <div className="haikus">
                        {haikuNodes}
                    </div>
                </main>
                <Footer/>
            </div>
        );
    }
});
