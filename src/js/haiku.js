var React = require('react');
var ReactDOM = require('react-dom');
var ReactRouter = require('react-router');
var Link = ReactRouter.Link;

module.exports = React.createClass({
    render: function(){
        var haiku = this.props.haiku.haiku.split("/").map(function(haikuString, key){
            return (
                <span key={key}>
                    {haikuString}
                    <br/>
                </span>
            )
        });
        return (
            <div className="ph3 ph5-ns pv5 haiku">
                <p className="f2 mv3 lh-title">
                    <Link to={`/haiku/${this.props.haiku.id}`} className="link color-inherit dim">
                        {haiku}
                    </Link>
                </p>
            </div>
        );
    }
});
