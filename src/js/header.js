var React = require('react');
var ReactDOM = require('react-dom');

module.exports = React.createClass({
    render: function(){
        return (
            <header className='ph3 ph5-ns pv4'>
                <h1 className="f1">Some Kind of Haiku</h1>
                <p className="f3 w-100 w-100-m w-60-ns lh-title">
                    I don't know whether to call these haikus or short poems or the writing equivalent of a doodle but whatever they may be, they stand as an expression of me in seventeen syllables.
                </p>
                <hr className="w3 ma0"/>
            </header>
        );
    }
});
