var React = require('react');
var ReactDOM = require('react-dom');
var data = require('./data');
var ReactRouter = require('react-router');
var Link = ReactRouter.Link;

module.exports = React.createClass({
    render: function(){
        var haiku = data.filter(function(item){
            return item.id == this.props.params.haikuId;
        }, this);
        haiku = haiku[0].haiku.split("/").map(function(haikuString, key){
            return (
                <span key={key}>
                    {haikuString}
                    <br/>
                </span>
            )
        });
        return (
            <div className="dt w-100 h-v-100">
                <div className="ph3 ph5-ns pv4 dtc v-mid tc">
                    <p className="f2 mv4 pv4 lh-title">
                        {haiku}
                    </p>
                    <p>
                        <Link to={`/`} className="link color-inherit dim underline">More haikus</Link>
                    </p>
                </div>
            </div>
        );
    }
});
