var React = require('react');
var ReactDOM = require('react-dom');
var HaikusList = require('./haikusList');
var Haiku = require('./haiku');
var singleHaiku = require('./singlehaiku');
var ReactRouter = require('react-router');
var Router = ReactRouter.Router;
var Route = ReactRouter.Route;
var useRouterHistory = ReactRouter.useRouterHistory;
var History = require('history')
var createHashHistory = History.createHashHistory;
var histories = useRouterHistory(createHashHistory)({ queryKey: false });

window.onload = init;

function init(){
    ReactDOM.render(
        <Router history={histories}>
          <Route path="/" component={HaikusList} />
          <Route path="/haiku/:haikuId" component={singleHaiku}/>
        </Router>,
        document.getElementById('root')
    )
}
